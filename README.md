# Google map to search the nearby restaurants

Use Google API to search nearby restaurants, display the walking direction and marker in Google Map.

  - Restaurant listing
  - Restaurant detail
  - Ordering by distance, cost, rating.
  - Filtering by opening hours.
  - Walking Direction to the restaurant

### Google Map API

Use Google Map Place API, after loaded api will auto call initMap to initial Google Map and services.
```sh
function initMap() {}
```

### Search nearby restaurants

Use Google Map PlacesService to search nearby restaurants. Mark down in Google Map and display in the list order by distance default.
  - distance, cost smaller first
  - rating bigger first
  - no data last

```sh
function searchNearbyRestaurants(location){}
```

We set radius to 500 (m) and type to restaurant
```sh
var request = {
    location: location,
    radius: '500',
    type: ['restaurant']
};
```

### Display restaurant detail

Click the restaurant marker or list will display restaurant detail in Google Map, the information include:
  - restaurant name
  - restaurant icon
  - restaurant opening status (Open Now / Closed)
  - restaurant address
  - restaurant price level
  - restaurant rating
  - restaurant walking distance from current location

```sh
function restaurantClick(th){}

function createInfo(place, m_infowindow, map){}
```
We can get more details by the place_id which request is an HTTP URL of the following form:
```sh
https://maps.googleapis.com/maps/api/place/details/output?parameters
```
** Notes: Use the api request need publish the site to internet, otherwise return CORS **

### Refreshing listing when map boundary is updated

Click the map will update the current location and auto refresh the restaurant list.

```sh
google.maps.event.addListener(map,'click',function(event) {})
```

### Direction to the restaurant

Click the marker or restaurants list will display the walking direction to the restaurant in Google Map.

```sh
function renderRouter(origin, destination){}
```

For the router, Use Google Map directionsService and select DirectionsTravelMode.WALKING
```sh
directionsService.route(request, function(response, status) {}
```

### Distance to the restaurant

Because the place service not include the disdance default, We need calculate the disdance from current location to each restaurant.

```sh
function computeDistanceAndOpening(origin, destinations){}
```

### Todos


License
----

MIT